f = open("4elt.serial", "r")
lines = f.readlines()
f.close()

f = open("4elt.edgelist", "w")
for line in lines:
    line = line.strip()
    x = " ".join(line.split(" ")[::-1])
    f.write(line)
    f.write("\n")
    f.write(x)
    f.write("\n")
f.close()
