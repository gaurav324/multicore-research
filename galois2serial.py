f = open("4elt.edgelist", "r")
lines = f.readlines()
f.close()

unique = {}
for line in lines:
    a = " ".join(map(lambda y: str(y), sorted(map(lambda x: int(x), line.split(" ")))))
    if a  not in unique:
        unique[a] = 1

f = open("4elt.bin", "w")
for key in unique:
   f.write(key)
   f.write("\n")
f.close()
